#!/bin/bash

BASE_PATH=`pwd`
BUILD_PATH="/tmp/mongo-cxx-driver"

# Create temp directory for building
mkdir -p $BUILD_PATH

# Clone the mongo driver into a tmp directory
git clone https://github.com/mongodb/mongo-cxx-driver.git $BUILD_PATH

cd $BUILD_PATH

# Start building the driver
scons --prefix=/usr/local --sharedclient install

cd $BASE_PATH

# Remove our build files
rm -rf $BUILD_PATH
