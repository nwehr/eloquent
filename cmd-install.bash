#!/bin/bash

BASE_PATH=`pwd`

EXT_PATH=( "eloquent-core" \
"eloquent-ext-consolewriter" \
"eloquent-ext-datagramreader" \
"eloquent-ext-datagramwriter" \
"eloquent-ext-filereader" \
"eloquent-ext-filewriter" \
"eloquent-ext-httpreader" \
"eloquent-ext-mongowriter" \
"eloquent-filt-regex" \
"eloquent-filt-tojson" 
)

# Make build directories and build/install the projects
for SUB_PATH in "${EXT_PATH[@]}"
do
	mkdir -p "$BASE_PATH/$SUB_PATH/build"
	cd "$BASE_PATH/$SUB_PATH/build"
	cmake ../
	make install
done

cd $BASE_PATH

# Clean up the build directories
for SUB_PATH in "${EXT_PATH[@]}"
do
	rm -rf "$BASE_PATH/$SUB_PATH/build"
done
